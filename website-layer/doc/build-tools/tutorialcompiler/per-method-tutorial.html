<!DOCTYPE html>

<html lang="en" data-content_root="../../">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>“Per-method” tutorials &#8212; Pytch  documentation</title>
    <link rel="stylesheet" type="text/css" href="../../_static/pygments.css?v=03e43079" />
    <link rel="stylesheet" type="text/css" href="../../_static/classic.css?v=36340f97" />
    <link rel="stylesheet" type="text/css" href="../../_static/css/pytch-classic.css?v=0321735e" />
    
    <script src="../../_static/documentation_options.js?v=7f41d439"></script>
    <script src="../../_static/doctools.js?v=9bcbadda"></script>
    <script src="../../_static/sphinx_highlight.js?v=dc90522c"></script>
    
    <link rel="icon" href="../../_static/favicon.ico"/>
    <link rel="author" title="About these documents" href="../../about.html" />
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link rel="next" title="Unit testing" href="unit-testing.html" />
    <link rel="prev" title="Tutorial metadata" href="tutorial-metadata.html" /> 
  </head><body>
<div class="NavBar">
  <a href="../../../app/"><h1>Pytch</h1></a>
  <ul>
    <a href="https://pytch.scss.tcd.ie/"><li>About Pytch</li></a>
    <a href="../../index.html"><li>Help</li></a>
    <a href="../../../app/tutorials/"><li>Tutorials</li></a>
    <a href="../../../app/my-projects/"><li>My projects</li></a>
  </ul>
</div>
<div class="warning-work-in-progress">
  <p>These help pages are incomplete — we are working on it!</p>
</div>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="per-method-tutorials">
<h1>“Per-method” tutorials<a class="headerlink" href="#per-method-tutorials" title="Link to this heading">¶</a></h1>
<p>We support tutorials for program represented in the “per-method” style
(also known internally as “structured” or “Pytch junior”).  The
underlying source of truth for such a tutorial remains a linear git
history for a single file of flat Python code, coupled with a markdown
file for the tutorial text.  For these tutorials, we also bring in the
idea of progressive revelation of help for the user, and a different
presentation of changes to the code.</p>
<section id="writing-a-per-method-tutorial">
<h2>Writing a per-method tutorial<a class="headerlink" href="#writing-a-per-method-tutorial" title="Link to this heading">¶</a></h2>
<p>The learner will work with the program using the per-method
representation and interface.  However, the source of truth for the
tutorial is a normal flat Pytch program, obeying some constraints to
ensure that it has a direct equivalent per-method representation.</p>
<p>Each well-defined change to the user’s program should be a commit in
the git history.  Such commits can be as short as only one or two
lines long for introductory tutorials.  Every commit must be of a
known and well-defined semantic kind, described below.</p>
<p>Each commit is presented as part of a “learner task” in the front end.
The learner tasks are marked up in the tutorial source by means of
begin and end shortcodes:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">{{&lt;</span> <span class="pre">learner-task</span> <span class="pre">&gt;}}</span></code> to mark the beginning of a learner task</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">{{&lt;</span> <span class="pre">/learner-task</span> <span class="pre">&gt;}}</span></code> to mark the end of a learner task</p></li>
</ul>
<p>In between these two shortcode markers, the learner task consists of
<em>introductory text</em> and a sequence of one or more <em>learner-task help</em>
sections.  The introductory text starts at the start of the learner
task, and ends just before the first <code class="docutils literal notranslate"><span class="pre">{{&lt;</span> <span class="pre">learner-task-help</span> <span class="pre">&gt;}}</span></code>
marker shortcode.  The text should describe in fairly broad,
high-level terms the goal of the change.</p>
<p>Each <code class="docutils literal notranslate"><span class="pre">{{&lt;</span> <span class="pre">learner-task-help</span> <span class="pre">}}</span></code> shortcode indicates the start of a
learner help section.  The help sections should provide more and more
detail about how to achieve the goal.  The last learner help section
should include “the answer”, i.e., tell the learner exactly what
change to their program is needed.  This is done by referring to a
tagged commit using a shortcode like</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">{{&lt;</span> <span class="pre">jr-commit</span> <span class="pre">COMMIT-SLUG</span> <span class="pre">COMMIT-KIND</span> <span class="pre">OPTIONAL-COMMIT-ARGS</span> <span class="pre">&gt;}}</span></code></p></li>
</ul>
<p>where:</p>
<ul class="simple">
<li><p><em>COMMIT-SLUG</em> is the label identifying this commit, as found in its
special tag commit message (e.g., for a commit with message
<code class="docutils literal notranslate"><span class="pre">{#increment-score}</span></code>, the <em>commit-slug</em> is <code class="docutils literal notranslate"><span class="pre">increment-score</span></code>);</p></li>
<li><p><em>COMMIT-KIND</em> identifies the kind of commit this is, for example one
which adds a new script to a sprite; see below for details of the
different kinds of commit;</p></li>
<li><p><em>OPTIONAL-COMMIT-ARGS</em> is JSON representing an array of the
arguments the particular commit-kind requires; see below for
details.</p></li>
</ul>
<section id="generating-learner-tasks-for-all-code-commits">
<h3>Generating learner-tasks for all code commits<a class="headerlink" href="#generating-learner-tasks-for-all-code-commits" title="Link to this heading">¶</a></h3>
<p>The tool <code class="docutils literal notranslate"><span class="pre">pytchbuild-emit-commit-slugs-markdown</span></code> will emit, to
stdout, markdown text consisting of a sequence of learner-task blocks,
one per code commit in the history.  This can be copied into the
<code class="docutils literal notranslate"><span class="pre">tutorial.md</span></code> file as a basis for writing the tutorial text.</p>
</section>
<section id="kinds-of-commit">
<h3>Kinds of commit<a class="headerlink" href="#kinds-of-commit" title="Link to this heading">¶</a></h3>
<p>Currently the tutorial author must indicate what kind of change is
being made by each commit, by supplying one of the following
commit-kinds.  The <code class="docutils literal notranslate"><span class="pre">add-medialib-appearance</span></code> kind takes one
argument; the other kinds take no arguments.  If the
<em>optional-commit-args</em> string is absent, the empty array is used.</p>
<p><code class="docutils literal notranslate"><span class="pre">add-sprite</span></code></p>
<blockquote>
<div><p>Add a new sprite to the project.  Such a commit should add code
text for a new Sprite-derived class whose body is the assignment
statement <code class="docutils literal notranslate"><span class="pre">Costumes</span> <span class="pre">=</span> <span class="pre">[]</span></code></p>
</div></blockquote>
<p><code class="docutils literal notranslate"><span class="pre">add-medialib-appearance</span></code> <em>DISPLAY-IDENTIFIER</em></p>
<blockquote>
<div><p>Add an element to the list of Costumes (for a Sprite) or Backdrops
(for the Stage).  Such a commit should add a string literal to the
appropriate class variable.  The <em>DISPLAY-IDENTIFIER</em> is a string
shown to the learner to help them find the correct appearance in
the media library.</p>
</div></blockquote>
<p><code class="docutils literal notranslate"><span class="pre">add-medialib-appearances-entry</span></code> <em>ENTRY-NAME</em></p>
<blockquote>
<div><p>Add multiple elements to the list of Costumes (for a Sprite) or
Backdrops (for the Stage).  Such a commit should add multiple
string literals to the appropriate class variable.  The
<em>ENTRY-NAME</em> is a string shown to the learner to help them find
the correct ‘entry’ (group of appearances) in the media library.
It is assumed that the named medialib entry does in fact contain
all the added images (and no others).</p>
</div></blockquote>
<p><code class="docutils literal notranslate"><span class="pre">delete-appearance</span></code></p>
<blockquote>
<div><p>Remove an element from the list of Costumes or Backdrops.  Such a
commit should remove exactly one string literal from the
appropriate class variable.  The deleted string literal is shown
to the user so they can find the right appearance to delete.</p>
</div></blockquote>
<p><code class="docutils literal notranslate"><span class="pre">add-script</span></code></p>
<blockquote>
<div><p>Add a script (decorated method) to a sprite.  The code added
should include the decorator, the method <code class="docutils literal notranslate"><span class="pre">def</span></code> line, and a body.
If the body is just <code class="docutils literal notranslate"><span class="pre">pass</span></code>, this is treated as empty; otherwise,
the displayed help will include the given body code.</p>
</div></blockquote>
<p><code class="docutils literal notranslate"><span class="pre">edit-script</span></code></p>
<blockquote>
<div><p>Change exactly one script (decorated method) body in exactly one
Sprite or the Stage.</p>
</div></blockquote>
<p><code class="docutils literal notranslate"><span class="pre">change-hat-block</span></code></p>
<blockquote>
<div><p>Change exactly one script’s decorator in exactly one Sprite or the
Stage.</p>
</div></blockquote>
</section>
<section id="representation-of-commits">
<h3>Representation of commits<a class="headerlink" href="#representation-of-commits" title="Link to this heading">¶</a></h3>
<p>There is almost a one-to-one mapping between the above <em>commit-kind</em>
strings and the values of the <code class="docutils literal notranslate"><span class="pre">kind</span></code> slot of the various
<code class="docutils literal notranslate"><span class="pre">CommitKind</span></code> types.  The exception is that
<code class="docutils literal notranslate"><span class="pre">add-medialib-appearance</span></code> and <code class="docutils literal notranslate"><span class="pre">add-medialib-appearances-entry</span></code> are
both mapped to <code class="docutils literal notranslate"><span class="pre">add-medialib-appearances-entry</span></code>, because they’re
treated the same by the front end.</p>
</section>
<section id="excluding-chapters-from-progress-trail">
<h3>Excluding chapters from “progress trail”<a class="headerlink" href="#excluding-chapters-from-progress-trail" title="Link to this heading">¶</a></h3>
<p>The front end shows a progress trail of circles which are replaced
with check-marks as the learner works through the tutorial.  Often the
last chapter or two are not really part of the tutorial proper; they
might be challenges to try, or the credits for the assets used.  To
indicate that these chapters should not be counted as part of the
progress the learner must make to complete the tutorial, the tutorial
author can include the shortcode</p>
<p><code class="docutils literal notranslate"><span class="pre">{{&lt;</span> <span class="pre">exclude-from-progress-trail</span> <span class="pre">&gt;}}</span></code></p>
<p>somewhere in that chapter.  Straight after the heading is a good
place.  Currently, all excluded chapters must be at the end of the
tutorial.</p>
</section>
</section>
<section id="summary-theory-of-operation-for-compiler">
<h2>Summary theory of operation for compiler<a class="headerlink" href="#summary-theory-of-operation-for-compiler" title="Link to this heading">¶</a></h2>
<p>An instance of the class <code class="docutils literal notranslate"><span class="pre">StructuredPytchProgram</span></code> wraps a
well-formed Pytch program, and provides access to the program’s list
of actors, methods, appearances, etc.  An instance of the class
<code class="docutils literal notranslate"><span class="pre">StructuredPytchDiff</span></code> can provide a “rich commit” instance
representing the change from an “old” state of the program to a “new”
state.  There are different rich-commit classes, one for each kind of
commit (e.g., <code class="docutils literal notranslate"><span class="pre">JrCommitAddSprite</span></code> for the <code class="docutils literal notranslate"><span class="pre">&quot;add-sprite&quot;</span></code>
commit-kind).  These form the interface between the compiler and the
front end.</p>
<p>There are fairly extensive assertions when generating these commits
that the code has undergone the right kind of change.  (E.g., an
“edit-script” commit must change exactly one method-body.)  If any of
these assertions fails, an exception is raised.</p>
<p>A learner task’s flat sequence of <code class="docutils literal notranslate"><span class="pre">div</span></code> elements, as produced by the
markdown processor, is converted into a nested form before being
written to the final tutorial output.  Each learner-task <code class="docutils literal notranslate"><span class="pre">div</span></code> will
contain a “commit <code class="docutils literal notranslate"><span class="pre">div</span></code>” with the JSON of the rich commit object as
a data attribute.</p>
</section>
<section id="future-work">
<h2>Future work<a class="headerlink" href="#future-work" title="Link to this heading">¶</a></h2>
<p>It should be possible to determine the commit-kind automatically.  One
approach would be to just try asking the <code class="docutils literal notranslate"><span class="pre">StructuredPytchDiff</span></code>
object for every kind of rich-commit kind.  Exactly one such request
should succeed, with all others raising an exception.</p>
<p>As we write more tutorials, other kinds of commit might be needed.
For example, one which adds all the images in a group (e.g., all four
directions of the player’s character in Qbert).</p>
<p>It would be good to include costume thumbnails in the “task cards”;
this might be made easier with help from the compiler.</p>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper"><ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../../webapp/user/index.html">Using the Pytch web app</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../vm/user/index.html">Writing Pytch programs</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../about.html">About Pytch</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../contact.html">Contact</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="../../developer.html">Developer documentation</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="../../developer/development-setup.html">Development setup</a></li>
<li class="toctree-l2"><a class="reference internal" href="../../developer/design-overview.html">Design overview</a></li>
<li class="toctree-l2"><a class="reference internal" href="../../vm/developer/index.html">VM</a></li>
<li class="toctree-l2"><a class="reference internal" href="../../webapp/developer/index.html">Webapp</a></li>
<li class="toctree-l2"><a class="reference internal" href="../../medialib/developer/index.html">Media library</a></li>
<li class="toctree-l2"><a class="reference internal" href="../../developer/index.html">Website</a></li>
<li class="toctree-l2 current"><a class="reference internal" href="../index.html">Tools</a><ul class="current">
<li class="toctree-l3 current"><a class="reference internal" href="../index.html#tutorial-compiler-and-related-tools">Tutorial compiler and related tools</a></li>
<li class="toctree-l3"><a class="reference internal" href="../index.html#assembly-of-website-bundle">Assembly of website bundle</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="../../source-build.html">Source and build information</a></li>
<li class="toctree-l2"><a class="reference internal" href="../../releases/changelog.html">Changelog</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="../../legal/index.html">Legal information</a></li>
</ul>
<div class="docs-home-link"><hr>
  <ul>
    <li>
      <a href="../../index.html">Pytch help home</a>
    <li>
  </ul>
</div>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
  </body>
</html>