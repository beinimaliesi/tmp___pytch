<!DOCTYPE html>

<html lang="en" data-content_root="../../">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Defining costumes &#8212; Pytch  documentation</title>
    <link rel="stylesheet" type="text/css" href="../../_static/pygments.css?v=03e43079" />
    <link rel="stylesheet" type="text/css" href="../../_static/classic.css?v=36340f97" />
    <link rel="stylesheet" type="text/css" href="../../_static/css/pytch-classic.css?v=0321735e" />
    
    <script src="../../_static/documentation_options.js?v=7f41d439"></script>
    <script src="../../_static/doctools.js?v=9bcbadda"></script>
    <script src="../../_static/sphinx_highlight.js?v=dc90522c"></script>
    
    <link rel="icon" href="../../_static/favicon.ico"/>
    <link rel="author" title="About these documents" href="../../about.html" />
    <link rel="index" title="Index" href="../../genindex.html" />
    <link rel="search" title="Search" href="../../search.html" />
    <link rel="next" title="Defining backdrops" href="backdrop-specs.html" />
    <link rel="prev" title="Scratch hat blocks → Pytch decorators" href="hat-blocks.html" /> 
  </head><body>
<div class="NavBar">
  <a href="../../../app/"><h1>Pytch</h1></a>
  <ul>
    <a href="https://pytch.scss.tcd.ie/"><li>About Pytch</li></a>
    <a href="../../index.html"><li>Help</li></a>
    <a href="../../../app/tutorials/"><li>Tutorials</li></a>
    <a href="../../../app/my-projects/"><li>My projects</li></a>
  </ul>
</div>
<div class="warning-work-in-progress">
  <p>These help pages are incomplete — we are working on it!</p>
</div>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="defining-costumes">
<span id="costume-specifications"></span><h1>Defining costumes<a class="headerlink" href="#defining-costumes" title="Link to this heading">¶</a></h1>
<p>A Sprite has <em>Costumes</em> and the Stage has <em>Backdrops</em>.  Both Sprites
and the Stage can have <em>Sounds</em>.  The way that you define these in
Pytch is quite flexible, but for most situations you will be able to
use a very simple approach.</p>
<p>Suppose you have added an image called <code class="docutils literal notranslate"><span class="pre">happy-kitten.jpg</span></code> to your
project, and you want to use this image as a costume for a sprite
<code class="docutils literal notranslate"><span class="pre">Kitten</span></code>.  You can do so like this:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span><span class="w"> </span><span class="nn">pytch</span>

<span class="k">class</span><span class="w"> </span><span class="nc">Kitten</span><span class="p">(</span><span class="n">pytch</span><span class="o">.</span><span class="n">Sprite</span><span class="p">):</span>
    <span class="n">Costumes</span> <span class="o">=</span> <span class="p">[</span><span class="s2">&quot;happy-kitten.jpg&quot;</span><span class="p">]</span>
</pre></div>
</div>
<p>This brings in your <code class="docutils literal notranslate"><span class="pre">happy-kitten.jpg</span></code> file as a costume for the
<code class="docutils literal notranslate"><span class="pre">Kitten</span></code> Sprite.  Pytch automatically switches to the first Costume
when building and launching the project, showing it in the centre of
the Stage.</p>
<section id="advanced-costume-specifications">
<h2>Advanced Costume specifications<a class="headerlink" href="#advanced-costume-specifications" title="Link to this heading">¶</a></h2>
<p>Sometimes you need more control over how Pytch uses your image as a
costume.  You can use the following variations independently for each
costume of a sprite.</p>
<section id="specifying-the-origin-of-the-costume">
<h3>Specifying the origin of the costume<a class="headerlink" href="#specifying-the-origin-of-the-costume" title="Link to this heading">¶</a></h3>
<p>By default, Pytch chooses the centre of your image as the <em>origin</em> of
your Sprite.  When you say <code class="docutils literal notranslate"><span class="pre">go_to_xy(100,</span> <span class="pre">80)</span></code>, for example, it is
the <em>origin</em> of the costume which ends up at the position (100, 80).</p>
<p>Sometimes you would like a different point in your image to be the
origin.  You can tell Pytch this by using a <em>three-element tuple</em> as a
costume specification.  For example,</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span><span class="w"> </span><span class="nn">pytch</span>

<span class="k">class</span><span class="w"> </span><span class="nc">Spaceship</span><span class="p">(</span><span class="n">pytch</span><span class="o">.</span><span class="n">Sprite</span><span class="p">):</span>
    <span class="n">Costumes</span> <span class="o">=</span> <span class="p">[(</span><span class="s2">&quot;rocket.png&quot;</span><span class="p">,</span> <span class="mi">10</span><span class="p">,</span> <span class="mi">20</span><span class="p">)]</span>
</pre></div>
</div>
<p>will make the origin of the <code class="docutils literal notranslate"><span class="pre">rocket</span></code> costume be 10 pixels right and
20 pixels down from the top-left corner of the image.  <em>Notice that
the ‘y’ coordinate goes down, rather than the usual ‘positive is up’
convention for the stage.  This is to follow the usual convention for
computer images, which uses (0, 0) as the top-left corner.</em></p>
</section>
<section id="specifying-the-label-of-the-costume">
<h3>Specifying the label of the costume<a class="headerlink" href="#specifying-the-label-of-the-costume" title="Link to this heading">¶</a></h3>
<p>In this example:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span><span class="w"> </span><span class="nn">pytch</span>

<span class="k">class</span><span class="w"> </span><span class="nc">RacingCar</span><span class="p">(</span><span class="n">pytch</span><span class="o">.</span><span class="n">Sprite</span><span class="p">):</span>
    <span class="n">Costumes</span> <span class="o">=</span> <span class="p">[</span><span class="s2">&quot;car.png&quot;</span><span class="p">,</span> <span class="s2">&quot;crash.png&quot;</span><span class="p">]</span>
</pre></div>
</div>
<p>you will be able to say <code class="docutils literal notranslate"><span class="pre">self.switch_costume(&quot;car&quot;)</span></code> or
<code class="docutils literal notranslate"><span class="pre">self.switch_costume(&quot;crash&quot;)</span></code> in a <code class="docutils literal notranslate"><span class="pre">RacingCar</span></code> method.  Pytch
uses the <em>stem</em> of the filename as the costume’s label.  You might
want to refer to these costumes by different <em>labels</em> instead.  To do
that, give a <em>two-element tuple</em> for a costume, like this:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span><span class="w"> </span><span class="nn">pytch</span>

<span class="k">class</span><span class="w"> </span><span class="nc">RacingCar</span><span class="p">(</span><span class="n">pytch</span><span class="o">.</span><span class="n">Sprite</span><span class="p">):</span>
    <span class="n">Costumes</span> <span class="o">=</span> <span class="p">[</span>
        <span class="p">(</span><span class="s2">&quot;fast-car&quot;</span><span class="p">,</span> <span class="s2">&quot;car.png&quot;</span><span class="p">),</span>
        <span class="p">(</span><span class="s2">&quot;crashed-car&quot;</span><span class="p">,</span> <span class="s2">&quot;crash.png&quot;</span><span class="p">),</span>
    <span class="p">]</span>
</pre></div>
</div>
<p>With these specifications, you now say
<code class="docutils literal notranslate"><span class="pre">self.switch_costume(&quot;fast-car&quot;)</span></code> or
<code class="docutils literal notranslate"><span class="pre">self.switch_costume(&quot;crashed-car&quot;)</span></code> in your code.</p>
</section>
<section id="specifying-the-label-and-the-origin-of-the-costume">
<span id="costume-label-origin-specifications"></span><h3>Specifying the label and the origin of the costume<a class="headerlink" href="#specifying-the-label-and-the-origin-of-the-costume" title="Link to this heading">¶</a></h3>
<p>If you want to control both the label and the origin, give a
<em>four-element tuple</em>, such as:</p>
<div class="highlight-python notranslate"><div class="highlight"><pre><span></span><span class="kn">import</span><span class="w"> </span><span class="nn">pytch</span>

<span class="k">class</span><span class="w"> </span><span class="nc">Hero</span><span class="p">(</span><span class="n">pytch</span><span class="o">.</span><span class="n">Sprite</span><span class="p">):</span>
    <span class="n">Costumes</span> <span class="o">=</span> <span class="p">[(</span><span class="s2">&quot;running&quot;</span><span class="p">,</span> <span class="s2">&quot;running-person.jpg&quot;</span><span class="p">,</span> <span class="mi">25</span><span class="p">,</span> <span class="mi">30</span><span class="p">)]</span>
</pre></div>
</div>
<p>This will give a costume based on the image <code class="docutils literal notranslate"><span class="pre">running-person.jpg</span></code>,
which you will switch to with <code class="docutils literal notranslate"><span class="pre">self.switch_costume(&quot;running&quot;)</span></code>, and
whose origin is the point 25 pixels from the left and 30 pixels from
the top of the image.</p>
</section>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="Main">
        <div class="sphinxsidebarwrapper"><ul class="current">
<li class="toctree-l1"><a class="reference internal" href="../../webapp/user/index.html">Using the Pytch web app</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="index.html">Writing Pytch programs</a><ul class="current">
<li class="toctree-l2"><a class="reference internal" href="sprite.html">Sprites</a></li>
<li class="toctree-l2"><a class="reference internal" href="stage.html">The stage</a></li>
<li class="toctree-l2"><a class="reference internal" href="non-hat-blocks.html">Scratch blocks → Pytch functions and methods</a></li>
<li class="toctree-l2"><a class="reference internal" href="pytch-module-functions.html">Functions in the pytch module</a></li>
<li class="toctree-l2"><a class="reference internal" href="hat-blocks.html">Scratch hat blocks → Pytch decorators</a></li>
<li class="toctree-l2 current"><a class="current reference internal" href="#">Defining costumes</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#advanced-costume-specifications">Advanced Costume specifications</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="backdrop-specs.html">Defining backdrops</a></li>
<li class="toctree-l2"><a class="reference internal" href="sound-specs.html">Defining sounds</a></li>
<li class="toctree-l2"><a class="reference internal" href="multi-threading.html">Running many scripts at once</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="../../about.html">About Pytch</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../contact.html">Contact</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../developer.html">Developer documentation</a></li>
<li class="toctree-l1"><a class="reference internal" href="../../legal/index.html">Legal information</a></li>
</ul>
<div class="docs-home-link"><hr>
  <ul>
    <li>
      <a href="../../index.html">Pytch help home</a>
    <li>
  </ul>
</div>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
  </body>
</html>